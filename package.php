#!/usr/bin/env php
<?php

$commands = [
    'vagrant destroy --force',
    'vagrant up',
    'vagrant halt',
    'vagrant package',
    'vagrant destroy --force',
];

$boxes = [
    'etable'         => array_merge(['vagrant box update'], $commands),
    'xenial64'       => array_merge(['vagrant box update'], $commands),
    'psql'           => $commands,
    'webserver'      => $commands,
    'webserver-psql' => $commands,
    'webserver-wp'   => $commands,
];

foreach ($boxes as $box => $commands) {
    $dir_box  = __DIR__ . '/boxes/' . $box;
    $path_box = __DIR__ . '/output/' . $box . '.box';

    chdir($dir_box);

    echo $box . ': building' . PHP_EOL;

    foreach ($commands as $command) {
        system($command);
    }

    rename($dir_box . '/package.box', $path_box);

    echo $box . ': ' . realpath($path_box) . PHP_EOL;
}

chdir(__DIR__);