# Clean apt

sudo apt-get -y autoremove
sudo apt-get autoclean

# Cleanup bash history

unset HISTFILE
[ -f /root/.bash_history ] && rm /root/.bash_history
[ -f /home/vagrant/.bash_history ] && rm /home/vagrant/.bash_history
 
# Cleanup log files

find /var/log -type f | while read f; do echo -ne '' > $f; done
 
# Swap

swappart=$(cat /proc/swaps | grep -v Filename | tail -n1 | awk -F ' ' '{print $1}')
if [ "$swappart" != "" ]; then
  swapoff $swappart;
  dd if=/dev/zero of=$swappart;
  mkswap $swappart;
  swapon $swappart;
fi


# Zero out disk

dd if=/dev/zero of=/EMPTY bs=1M 2>/dev/null
rm -f /EMPTY

sync
sync
sync