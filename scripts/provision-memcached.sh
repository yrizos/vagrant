#!/usr/bin/env bash

sudo apt-get -y update

sudo apt-get -y install memcached
sudo apt-get -y install libmemcached-tools

sudo apt-get -y autoremove
sudo apt-get -y clean