#!/usr/bin/env bash

sudo apt-get update -y

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"

sudo apt-get install -y mysql-server

sudo apt-get -y autoremove
sudo apt-get -y clean

sudo sed -i "s/.*bind-address.*/# bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

echo "GRANT ALL ON *.* TO root@192.168.10.101 IDENTIFIED BY 'root';" | sudo mysql -uroot -proot
echo "GRANT ALL ON *.* TO root@localhost IDENTIFIED BY 'root';" | sudo mysql -uroot -proot
echo "GRANT ALL ON *.* TO root@'%' IDENTIFIED BY 'root';" | sudo mysql -uroot -proot
echo "FLUSH PRIVILEGES;" | sudo mysql -uroot -proot
echo "CREATE DATABASE woo;" | sudo mysql -uroot -proot

sudo systemctl restart mysql.service