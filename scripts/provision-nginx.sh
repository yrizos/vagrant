#!/usr/bin/env bash

sudo apt-get -y update

sudo apt-get -y install nginx 

sudo apt-get -y autoremove
sudo apt-get -y clean

sudo systemctl stop nginx

sudo mkdir -p /vagrant/public

cat << EOF | sudo tee /etc/nginx/sites-enabled/default

server {
	listen 80 default_server;
	listen [::]:80 default_server;

    charset utf-8;

	root /vagrant/public;
		
	index index.php;

	server_name _;

	location / {
		try_files \$uri \$uri/ /index.php?\$query_string;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;

		fastcgi_pass unix:/run/php/php7.1-fpm.sock;
	}

    location ~* ^.+\.(?:css|cur|js|jpe?g|gif|htc|ico|png|html|xml|otf|ttf|eot|woff|svg)$ {
        access_log off;
        expires 30d;
        tcp_nodelay off;

        ## Set the OS file cache.
        open_file_cache max=3000 inactive=120s;
        open_file_cache_valid 45s;
        open_file_cache_min_uses 2;
        open_file_cache_errors off;
    }

	location ~ /\.ht {
		deny all;
	}

	sendfile off;
}

EOF

sudo systemctl start nginx

cat << EOF | sudo tee /vagrant/public/index.php
<?php phpinfo();
EOF