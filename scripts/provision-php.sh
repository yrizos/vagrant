#!/usr/bin/env bash

sudo apt-get update -y

sudo apt-get -y install python-software-properties
sudo apt-get -y install mcrypt

sudo add-apt-repository -y ppa:ondrej/php

sudo apt-get -y update
sudo apt-get -y install php7.1

sudo apt-get -y install php7.1-fpm 
sudo apt-get -y install php7.1-pgsql
sudo apt-get -y install php7.1-mbstring 
sudo apt-get -y install php7.1-mcrypt 
sudo apt-get -y install php7.1-opcache
sudo apt-get -y install php7.1-curl 
sudo apt-get -y install php7.1-intl 
sudo apt-get -y install php7.1-zip 
sudo apt-get -y install php7.1-gd 
sudo apt-get -y install php7.1-xml
sudo apt-get -y install php7.1-xdebug
sudo apt-get -y install php7.1-memcached
sudo apt-get -y install php7.1-memcache

sudo apt-get -y autoremove
sudo apt-get -y clean

cd ~

curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
sudo rm composer-setup.php

sudo sed -i "s/max_execution_time = .*/max_execution_time = 45/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 20M/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/post_max_size = .*/post_max_size = 64M/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL \& E_DEPRECATED /" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/cgi.fix_pathinfo = .*/cgi.fix_pathinfo = 1/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/allow_url_include = .*/allow_url_include = On/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/expose_php = .*/expose_php = Off/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/;date.timezone =/date.timezone = \"Europe\/Athens\"/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/session.save_handler = .*/session.save_handler = memcache/" /etc/php/7.1/fpm/php.ini
sudo sed -i "s/;session.save_path = .*/session.save_path = \"tcp:\/\/localhost:11211\"/" /etc/php/7.1/fpm/php.ini

sudo systemctl restart php7.1-fpm.service