#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y upgrade

sudo timedatectl set-timezone Europe/Athens

sudo apt-get install cachefilesd
sudo echo "RUN=yes" > /etc/default/cachefilesd

cat << EOF | sudo tee /home/vagrant/.bash_profile
alias ls="ls -la --color=auto"
EOF

